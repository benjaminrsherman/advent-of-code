count :: Int -> Int -> [[Char]] -> (Int, Int) -> Int
count _ _ [] _ = 0
count x y (row:rest) (dx, dy)
  = found + count newX (y + 1) rest (dx, dy)
      where
          pos = x `mod` length row
          newX = if y `mod` dy == 0
                    then x + dx
                    else x
          found = if y `mod` dy == 0 && (row !! pos == '#')
                     then 1
                     else 0

main = do
    content <- readFile "trees.txt"
    let trees = lines content
    putStrLn "Part 1:"
    print $ count 0 0 trees (3, 1)
    putStrLn "Part 2:"
    print $ product $ map (count 0 0 trees) [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
