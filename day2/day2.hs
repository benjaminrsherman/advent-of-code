import           Data.Char
import           Text.ParserCombinators.ReadP

data PasswordRow = PasswordRow String Char Int Int
    deriving Show

digit :: ReadP Char
digit = satisfy isDigit

index :: ReadP Int
index = fmap read (many1 digit)

readPasswordRow :: ReadP PasswordRow
readPasswordRow = do
    num1 <- index
    skipMany1 $ satisfy (== '-')
    num2 <- index
    skipSpaces
    letter <- get
    char ':'
    skipSpaces
    password <- look
    return $ PasswordRow password letter num1 num2

validPart1 :: PasswordRow -> Bool
validPart1 (PasswordRow p l n1 n2)
  = (letterCount >= n1) && (letterCount <= n2)
    where
        letterCount = length $ filter (== l) p

validPart2 :: PasswordRow -> Bool
validPart2 (PasswordRow p l n1 n2)
  = p1Valid /= p2Valid
    where
        p1Valid = p !! (n1 - 1) == l
        p2Valid = p !! (n2 - 1) == l

main = do
    content <- readFile "passwords.txt"
    let passwordRows = map (fst . head . readP_to_S readPasswordRow) (lines content)
    putStrLn "Part 1:"
    print $ length (filter validPart1 passwordRows)
    putStrLn "Part 2:"
    print $ length (filter validPart2 passwordRows)
