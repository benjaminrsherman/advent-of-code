def part1(n1: int, n2: int, letter: str, password: str):
    return password.count(letter) in range(n1, n2 + 1)


def part2(n1: int, n2: int, letter: str, password: str):
    b1 = password[n1 - 1] == letter
    b2 = password[n2 - 1] == letter
    return b1 != b2


p1ct = 0
p2ct = 0
with open("passwords.txt") as f:
    for line in f.readlines():
        minux_idx = line.index("-")
        space_idx = line.index(" ")
        colon_idx = line.index(":")

        n1 = int(line[:minux_idx])
        n2 = int(line[minux_idx + 1 : space_idx])
        letter = line[space_idx + 1]
        password = line[colon_idx + 2 :]

        if part1(n1, n2, letter, password):
            p1ct += 1

        if part2(n1, n2, letter, password):
            p2ct += 1

print(f"Part 1: {p1ct}")
print(f"Part 2: {p2ct}")
