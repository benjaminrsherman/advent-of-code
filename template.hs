import           Data.Char
import           Text.ParserCombinators.ReadP

-- Parsing helpers
digit :: ReadP Char
digit = satisfy isDigit

number :: Bool -> ReadP Int
number True  = fmap read (many1 digit)
number False = do
    minus <- option ' ' (char '-')
    num <- many1 digit
    return $ read (minus:num)

getParse :: ReadP a -> String -> a
getParse p = fst . head . readP_to_S p

-- Commented out because haskell doesn't like non-generic returns for generic types
-- part1 :: Show b => [a] -> b
part1 inp = "TODO"

-- Commented out because haskell doesn't like non-generic returns for generic types
--part2 :: Show b => [a] -> b
part2 inp = "TODO"

-- Commented out because haskell doesn't like non-generic returns for generic types
--parseLine :: String -> a
parseLine line = "TODO"

main = do
    content <- readFile "input.txt"
    let lineData = map parseLine (lines content)
    putStrLn "Part 1:"
    print $ part1 lineData
    putStrLn "Part 2:"
    print $ part2 lineData
