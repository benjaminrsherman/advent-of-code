import           Data.Char
import           Data.Set                     (Set, empty, fromList, insert,
                                               intersection, union)
import           Text.ParserCombinators.ReadP

personsQuestions :: String -> Set Char
personsQuestions = foldr insert empty

questionCount :: Set Char -> (Set Char -> Set Char -> Set Char) -> [String] -> Int
questionCount curr_questions _ [] = length curr_questions
questionCount curr_questions op (person:rest) = questionCount newSet op rest
    where newSet = curr_questions `op` personsQuestions person

countP1 :: [String] -> Int
countP1 = questionCount empty union

countP2 :: [String] -> Int
countP2 = questionCount (fromList ['a'..'z']) intersection

_splitLineGroups :: [[String]] -> [String] -> [String] -> [[String]]
_splitLineGroups listOfLists currList [] = listOfLists ++ [currList]

_splitLineGroups listOfLists currList ("":rest)
  = _splitLineGroups newLists [] rest
      where
          newLists = listOfLists ++ [currList]

_splitLineGroups listOfLists currList (currLine:rest)
  = _splitLineGroups listOfLists newCurrList rest
      where
          newCurrList = currList ++ [currLine]

splitLineGroups :: [String] -> [[String]]
splitLineGroups = _splitLineGroups [] []

main = do
    content <- readFile "input.txt"
    let lineData = splitLineGroups (lines content)
    putStrLn "Part 1:"
    print $ sum $ map countP1 lineData
    putStrLn "Part 2:"
    print $ sum $ map countP2 lineData
