from collections import defaultdict


def solve(input_file):
    data = []
    running = []
    for line in input_file:
        line = line.strip()
        if not line:
            data.append(running)
            running = []
        else:
            running.append(line)
    data.append(running)

    p1_counts = []
    p2_counts = []
    for group in data:
        group_counts = []

        for line in group:
            count = set()
            for char in line:
                count.add(char)
            group_counts.append(count)

        intersection = group_counts[0]
        union = group_counts[0]
        for count in group_counts[1:]:
            intersection = intersection.intersection(count)
            union = union.union(count)

        p1_counts.append(union)
        p2_counts.append(intersection)

    yield sum(map(len, p1_counts)), sum(map(len, p2_counts))


with open("input.txt", "r") as input_file:
    for solution in solve(input_file):
        print(solution)
