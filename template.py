import typing


def parse_line(line: str) -> str:
    raise NotImplementedError("TODO")


def solve(input_file: typing.IO) -> typing.Generator[str, None, None]:
    data = [parse_line(line.strip()) for line in input_file if line]
    yield "Solution not implemented"


with open("input.txt", "r") as input_file:
    for solution in solve(input_file):
        print(solution)
