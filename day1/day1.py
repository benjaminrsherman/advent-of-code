with open("entries.txt") as f:
    entries = list(map(int, f.readlines()))

print("Part 1:")
for i, entry in enumerate(entries):
    for j in range(i, len(entries)):
        if entry + entries[j] == 2020:
            print(entry * entries[j])

print("Part 2:")
for i, entry in enumerate(entries):
    for j in range(i, len(entries)):
        for k in range(j, len(entries)):
            if entry + entries[j] + entries[k] == 2020:
                print(entry * entries[j] * entries[k])
