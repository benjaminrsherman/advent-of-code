import           Data.Set hiding (map)

twoSum :: Set Integer -> Integer -> [Integer] -> Maybe Integer
twoSum _ _ [] = Nothing
twoSum s targetVal (h:t)  = if member targ s
                                then Just (h * targ)
                                else twoSum (insert h s) targetVal t
                            where targ = targetVal - h

threeSum :: Set Integer -> Integer -> [Integer] -> Maybe Integer
threeSum _ _ [] = Nothing
threeSum s targetVal (h:t)  = case twoSum s (targetVal - h) t of
                                Just found -> Just (found * h)
                                Nothing    -> threeSum (insert h s) targetVal t

main = do
    content <- readFile "entries.txt"
    let fileLines = map read (lines content)
    putStrLn "Part 1:"
    print $ twoSum empty 2020 fileLines
    putStrLn "Part 2:"
    print $ threeSum empty 2020 fileLines
